;;; GNU Guix --- Functional package management for GNU
;;; Copyright © 2019 Pierre Neidhardt <mail@ambrevar.xyz>
;;;
;;; This file is not part of GNU Guix.
;;;
;;; GNU Guix is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; GNU Guix is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with GNU Guix.  If not, see <http://www.gnu.org/licenses/>.

(define-module (games packages antichamber)
  #:use-module (ice-9 match)
  #:use-module (srfi srfi-1)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix utils)
  #:use-module (nonguix build-system binary)
  #:use-module (nonguix licenses)
  #:use-module (gnu packages)
  #:use-module (gnu packages base)
  #:use-module (gnu packages cpp)
  #:use-module (gnu packages gcc)
  #:use-module (gnu packages bash)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages elf)
  #:use-module (gnu packages gl)
  #:use-module (gnu packages linux)
  #:use-module (gnu packages sdl)
  #:use-module (gnu packages xiph)
  #:use-module (gnu packages xorg)
  #:use-module (games utils)
  #:use-module (nonguix build utils)
  #:use-module (games humble-bundle))

(define-public antichamber
  (let* ((version "1.01")
         (file-name (string-append "antichamber_1.01_linux_1392664980.sh")))
    (package
      (name "antichamber")
      (version version)
      (source
       (origin
         (method humble-bundle-fetch)
         (uri (humble-bundle-reference
               (help (humble-bundle-help-message name))
               (config-path (list (string->symbol name) 'key))
               (files (list file-name))))
         (file-name file-name)
         (sha256
          (base32
           "0pi3ih7bclyvrjk8hl742ikbh3fimilc6219w5f03vlmp7rsqws9"))))
      (build-system binary-build-system)
      (supported-systems '("i686-linux" "x86_64-linux"))
      (arguments
       `(#:system "i686-linux"
         #:patchelf-plan
         `(("data/x86/Binaries/Linux/UDKGame-Linux"
            ("gcc" "sdl2" "libogg" "libvorbis" "libgl" "tcmalloc"))
           ("data/x86/Binaries/Linux/lib/libPhysXCooking.so"
            ("gcc"))
           ("data/x86/Binaries/Linux/lib/libPhysXCore.so"
            ("gcc"))
           ("data/x86/Binaries/Linux/lib/libsteam_api.so"
            ("gcc")))
         #:install-plan
         `(("data/noarch" (".") "share/antichamber/")
           ("data/x86/Binaries/Linux"
            ("UDKGame-Linux" "steam_appid.txt ")
            "share/antichamber/Binaries/Linux/")
           ;; We keep the included SDL2_mixer since it seems to be patched.
           ;; Otherwise we get the following error:
           ;;   antichamber: symbol lookup error: antichamber: undefined symbol: MinorityMix_SetPosition
           ("data/x86/Binaries/Linux/lib/"
            ("libPhysXCooking.so" "libPhysXCore.so" "libsteam_api.so" "libSDL2_mixer-2.0.so.0")
            "share/antichamber/Binaries/Linux/lib/"))
         #:phases
         (modify-phases %standard-phases
           (replace 'unpack
             (lambda* (#:key inputs #:allow-other-keys)
               (invoke (which "makeself_safeextract")
                       "--mojo"
                       (assoc-ref inputs "source"))
               (chdir ((@@ (guix build gnu-build-system) first-subdirectory) "."))
               #t))
           (add-after 'install 'clean-up-permissions
             (lambda* (#:key outputs #:allow-other-keys)
               (let* ((out (assoc-ref outputs "out"))
                     (share (string-append out "/share/antichamber/")))
                 (for-each
                  (lambda (file) (chmod file #o644))
                  (append
                   (map (lambda (s) (string-append share s))
                        '("AntichamberIcon.bmp" "AntichamberIcon.png"
                          "BuildVersion.txt" "README.linux" "UpdateLog.txt"))
                   (find-files (string-append share "Engine") ".*")
                   (find-files (string-append share "UDKGame") ".*")))
                 (chmod (string-append share "Binaries/Linux/UDKGame-Linux")
                        #o755))
               #t))
           (add-after 'install 'fix-tcmalloc-path
             (lambda* (#:key inputs outputs #:allow-other-keys)
               (let* ((output (assoc-ref outputs "out"))
                      (tcmalloc (assoc-ref inputs "tcmalloc")))
                 (symlink (string-append tcmalloc "/lib/libtcmalloc.so.4")
                          (string-append output "/share/antichamber/Binaries/Linux/lib/libtcmalloc.so.0")))
               #t))
           (add-after 'install 'make-wrapper
             (lambda* (#:key inputs outputs #:allow-other-keys)
               (let* ((output (assoc-ref outputs "out"))
                      (wrapper (string-append output "/bin/antichamber"))
                      (real (string-append output "/share/antichamber/Binaries/Linux/UDKGame-Linux"))
                      (lib (string-append output "/share/antichamber/Binaries/Linux/lib"))
                      (icon (string-append output "/share/antichamber/noarch/AntichamberIcon.png")))
                 (make-wrapper wrapper real
                               `("LD_LIBRARY_PATH" ":" prefix
                                 ,(list lib)))
                 (make-desktop-entry-file (string-append output "/share/applications/antichamber.desktop")
                                          #:name "Antichamber"
                                          #:exec wrapper
                                          #:icon icon
                                          #:categories '("Application" "Game")))
               #t)))))
      (native-inputs
       `(("patchelf" ,patchelf)
         ("p7zip" ,p7zip)
         ("makeself-safeextract" ,makeself-safeextract)))
      (inputs
       `(("sdl2" ,sdl2)
         ("libogg" ,libogg)
         ("libvorbis" ,libvorbis)
         ("tcmalloc" ,gperftools)
         ("gcc" ,gcc "lib")
         ("libgl" ,mesa)))
      (home-page "http://www.antichamber-game.com/")
      (synopsis "Non-euclidian first-person puzzle-platform game")
      (description "Antichamber is a first-person puzzle-platform game.  Many of
the puzzles are based on phenomena that occur within impossible objects created
by the game engine, such as passages that lead the player to different locations
depending on which way they face, and structures that seem otherwise impossible
within normal three-dimensional space.  The game includes elements of
psychological exploration through brief messages of advice to help the player
figure out solutions to the puzzles as well as adages for real life.")
      (license (undistributable "No URL")))))
