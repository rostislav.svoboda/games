;;; GNU Guix --- Functional package management for GNU
;;; Copyright © 2019 Pierre Neidhardt <mail@ambrevar.xyz>
;;; Copyright © 2019 Efraim Flashner <efraim@flashner.co.il>
;;; Copyright © 2019 Alex Griffin <a@ajgrf.com>
;;;
;;; This file is not part of GNU Guix.
;;;
;;; GNU Guix is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; GNU Guix is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with GNU Guix.  If not, see <http://www.gnu.org/licenses/>.

(define-module (games packages ftl-faster-than-light)
  #:use-module (ice-9 match)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix utils)
  #:use-module (nonguix build-system binary)
  #:use-module (games build-system mojo)
  #:use-module (games gog-download)
  #:use-module (gnu packages)
  #:use-module (gnu packages base)
  #:use-module (gnu packages bash)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages elf)
  #:use-module (gnu packages gl)
  #:use-module (gnu packages linux)
  #:use-module (gnu packages xorg)
  #:use-module (games utils)
  #:use-module (nonguix build utils)
  #:use-module (games humble-bundle)
  #:use-module (nonguix licenses))

(define-public ftl-faster-than-light
  (let* ((version "1.6.12")
         (file-name (string-append "FTL." version ".Linux.zip"))
         (binary (match (or (%current-target-system)
                            (%current-system))
                   ("x86_64-linux" "FTL.amd64")
                   ("i686-linux" "FTL.x86")
                   (_ ""))))
    (package
      (name "ftl-faster-than-light")
      (version version)
      (source
       (origin
         (method humble-bundle-fetch)
         (uri (humble-bundle-reference
               (help (humble-bundle-help-message name))
               (config-path (list (string->symbol name) 'key))
               (files (list file-name))))
         (file-name file-name)
         (sha256
          (base32
           "1jbsrh0h4rwpbbdr3xq06ziw7idncqpg03yrhqhcqvh2d2js0w2s"))))
      (build-system binary-build-system)
      (supported-systems '("i686-linux" "x86_64-linux"))
      (arguments
       `(#:strip-binaries? #f             ; allocated section `.dynstr' not in segment
         #:patchelf-plan
         `((,,(string-append "data/" binary)
            ("libc" "alsa-lib" "libx11" "mesa")))
         #:install-plan
         `(("." ("FTL_README.html") "share/doc/ftl/")
           ("data" (,,binary) "share/ftl/ftl")
           ("data" ("ftl.dat") "share/ftl/")
           ("data" ("exe_icon.bmp")  "/share/pixmaps/ftl.bmp")
           ("data" ("licenses") "share/doc/ftl/"))
         #:phases
         (modify-phases %standard-phases
           (add-after 'install 'create-wrapper
             (lambda* (#:key inputs outputs #:allow-other-keys)
               (let* ((output (assoc-ref outputs "out"))
                      (wrapper (string-append output "/bin/" "ftl"))
                      (real (string-append output "/share/ftl/ftl")))
                 (make-wrapper wrapper real)
                 (chmod real #o755)
                 (make-desktop-entry-file (string-append output "/share/applications/ftl.desktop")
                                          #:name "FTL: Faster Than Light"
                                          #:exec wrapper
                                          #:icon (string-append output "/share/pixmaps/ftl.bmp")
                                          #:categories '("Application" "Game")))
               #t)))))
      (native-inputs
       `(("unzip" ,unzip)))
      (inputs
       `(("alsa-lib" ,alsa-lib)
         ("bash" ,bash-minimal)
         ("libx11" ,libx11)
         ("mesa" ,mesa)))
      (home-page "https://subsetgames.com/ftl.html")
      (synopsis "Space-based top-down real-time strategy roguelike game")
      (description "In FTL you experience the atmosphere of running a spaceship
trying to save the galaxy.  It's a dangerous mission, with every encounter
presenting a unique challenge with multiple solutions.

What will you do if a heavy missile barrage shuts down your shields?

@itemize
@item Reroute all power to the engines in an attempt to escape?
@item Power up additional weapons to blow your enemy out of the sky?
@item Or take the fight to them with a boarding party?
@end itemize

This \"spaceship simulation roguelike-like\" allows you to take your ship and
crew on an adventure through a randomly generated galaxy filled with glory and
bitter defeat.")
      (license (undistributable "No URL")))))

(define-public gog-ftl-faster-than-light
  (let ((buildno "35269")
        (binary (match (or (%current-target-system)
                           (%current-system))
                  ("x86_64-linux" "FTL.amd64")
                  ("i686-linux" "FTL.x86")
                  (_ ""))))
    (package
      (inherit ftl-faster-than-light)
      (name "gog-ftl-faster-than-light")
      (version "1.6.12.2")
      (source
       (origin
         (method gog-fetch)
         (uri "gogdownloader://faster_than_light/en3installer0")
         (file-name (string-append "ftl_advanced_edition_"
                                   (string-replace-substring version "." "_")
                                   "_" buildno ".sh"))
         (sha256
          (base32
           "1jg0cchab8kna4vyqxs93h2dxpsjgpkppql9g3vj7f27pz5vvj5a"))))
      (supported-systems '("i686-linux" "x86_64-linux"))
      (build-system mojo-build-system)
      (arguments
       (strip-keyword-arguments
        '(#:install-plan)
        (ensure-keyword-arguments
         (package-arguments ftl-faster-than-light)
         `(#:phases
           (modify-phases %standard-phases
             ;; The game built fine but yielded black screen upon startup (sound working!)
             ;; Turns out it is a more common issue and relates to the Mesa version
             ;; (for reference: https://subsetgames.com/forum/viewtopic.php?f=9&t=34411)
             ;; The wrapper takes care of this by setting MESA_GL_VERSION_OVERRIDE
             ;;
             ;; Affected video cards: Nvidia Geforce GTX 1650, AMD Radeon HD 7470
             (add-after 'install 'fix-mesa-version
               (lambda* (#:key inputs outputs #:allow-other-keys)
                 (let* ((out (assoc-ref outputs "out"))
                        (binary (string-append out "/bin/ftl-faster-than-light")))
                   (wrap-program binary '("MESA_GL_VERSION_OVERRIDE" = ("3.0"))))
                 #t)))))))
      (license (undistributable
                (string-append "file://data/noarch/docs/"
                               "End User License Agreement.txt"))))))
