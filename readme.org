#+TITLE: Non-free games for Guix

This is a [[https://www.gnu.org/software/guix/][Guix]] channel for non-free games.  It provides an easy "1-click"
installation process.  It does not provide non-public game data
however, you'll have to provide it yourself (e.g. from a disk or a local
installation).

(Check out the [[https://gitlab.com/Nonguix/nonguix][Nonguix channel]] if you're interested in nonfree, non-gaming
packages!)

Check out the [[games/packages][list of games]]!

* Manifesto

Guix Gaming Channels aim to be an answer to many issues with the gaming industry:

- Preservation :: Closed-source games don't stand the test of time as operating
                  systems get updated.  Guix makes it possible to specify the
                  exact set of dependencies (including library versions).  Thus, a
                  closed-source game properly packaged on Guix should run
                  forever.  (Minus hardware differences -- yet another
                  battle...)

- Ease of installation and upgrades :: The Guix Gaming Channels aim at
     streamlining the installation and upgrade processes, which should be no more
     difficult than installing any other package.  While gaming platforms like
     Steam do the job, they are non-free software and heavily centralized.
     Should Steam go down, the impact on the users and maybe the industry as a
     whole could be tremendous.  With Guix, the platform and game distribution
     is all in the public and decentralized onto the user machines.  Only the
     game data remains to be distributed somehow, but that will be a topic for
     another time.

- Extensive support for game modifications (/mods/) :: Some games ship
     development kits and enable communities to modify and extend the games.
     This content, known as /mods/, is often publicly available.  Mod
     installation and management can be tricky and varies from game to game.
     The Guix Gaming Channels provide a universal mod management tool to
     install, update and remove mods.

- Multi-user and roll-back support :: Among many other Guix niceties, the Guix
     Gaming channels allow multiple users to share the same installation of
     games on a machine.  If Alice does not want a game that Bob has, she can
     remove it from her profile, it will remain visible only to Bob.  Roll-backs
     are also supported: should you dislike an upgrade or should a mod break
     some game features, you can roll-back to any point in time.

- A far cry for more free software :: Video games are prime examples of software
     that suffer from a startling lack of openness.  The art form as a whole
     suffers tremendously.  We are hoping that this effort will help the
     industry follow the free software movement.

* Warning

The Guix Gaming Channels group does not endorse any non-free application.
We believe that this is non-ethical, harmful to software development and
restricts the users freedom.
See the [[https://www.gnu.org/philosophy/free-sw.en.html][GNU philosophy]] for a more thorough discussion.

We believe that free software is the right development model for video games.
This project only helps preserving existing non-free games and, hopefully,
raising awareness for more openness in the gaming industry.

You should understand the implication of using non-free software.  Some of those
implications include:

- Endorsement of non-free products and the perpetration of a culture of
  restriction on liberties.

- Non-free software cannot (or hardly) be audited: it can potentially spy on
  you, destroy or steal your data.

As a minimal security measure, it's heavily recommended to run any non-free
software inside a container.

* Installation

[[https://www.gnu.org/software/guix/download/][Install]] Guix.

Then the gaming packages can be added as a [[https://www.gnu.org/software/guix/manual/en/html_node/Channels.html][Guix channel]].  To do so, add it to
=~/.config/guix/channels.scm=:

#+BEGIN_SRC scheme
  (cons* (channel
          (name 'guix-gaming-games)
          (url "https://gitlab.com/guix-gaming-channels/games.git")
          ;; Enable signature verification:
          (introduction
           (make-channel-introduction
            "c23d64f1b8cc086659f8781b27ab6c7314c5cca5"
            (openpgp-fingerprint
             "50F3 3E2E 5B0C 3D90 0424  ABE8 9BDC F497 A4BB CC7F"))))
         %default-channels)
#+END_SRC

Then run ~guix pull~.

* Configuration

Most of our packages are commercial games which are not freely available, so
some additional configuration is required to tell Guix how to obtain the setup
files to build.  This may mean credentials for fetching from an online store,
or the path to a local file to use.

The configuration for Guix Gaming Channels is located at
=~/.config/guix-gaming-channels/games.scm=, or it may optionally be encrypted
with GPG at =~/.config/guix-gaming-channels/games.scm.gpg=.  The Scheme config
file must return a ~gaming-config~ record (see example below).  If you try to
build a package without the required configuration keys, you'll be greeted
with a helpful error message telling you what and how to add to your config.
Here's an example configuration:

#+BEGIN_SRC scheme
(define humble-bundle-key "ABCD1234")

(make-gaming-config
 `((gog ((email "my-gog-email@example.com")
         (password "password")))
   (factorio ((username "me")
              (token "abcdefg123456ABC")))
   ;; Humble Bundle order key:
   (ftl-faster-than-light ((key ,humble-bundle-key)))
   ("quake-3-arena" "/path/to/file")))
#+END_SRC

(Please be aware that when building packages, your credentials will be written
to =/gnu/store= in plain text!  This is an unfortunate technical limitation of
GNU Guix inherited from Nix.  The garbage collector can remove them, so
consider running ~guix gc~ after installing your games.)

* Avoiding Unnecessary Downloads

Currently, Guix always tries to fetch setup files remotely unless they are
already in =/gnu/store=.  To build using a local file instead, you
may find
[[https://guix.gnu.org/manual/en/html_node/Package-Transformation-Options.html][package transformation options]]
in Guix very helpful.  For example:

#+BEGIN_SRC bash
  $ guix install --with-source=game=</path/to/installer> game
#+END_SRC

Another option is adding the installer to =/gnu/store= manually:

#+BEGIN_SRC bash
  $ guix download </path/to/installer>
#+END_SRC

Running the garbage collector normally removes setup files from
=/gnu/store=, causing Guix to re-fetch them when you build a game again.
To prevent the garbage collector from removing a package's source, run:

#+BEGIN_SRC bash
  $ guix build --source --root=<file> game
#+END_SRC

A symlink will be created at <file>, which will prevent Guix from collecting
the package's source as long as it exists.

* Contributing

Contributions are welcome!  If there's a package you would like to add, just
fork the repository and create a Merge Request when your package is ready.
Keep in mind:

- Nonguix follows the same
  [[https://www.gnu.org/software/guix/manual/en/html_node/Coding-Style.html][coding style]]
  as GNU Guix.  If you don't use Emacs, you should make use of the indent
  script from the GNU Guix repository (=./etc/indent-code.el=).
- Commit messages should follow the same
  [[https://www.gnu.org/prep/standards/html_node/Change-Logs.html][conventions]]
  set by GNU Guix.
- Although licensing restrictions are relaxed, packages should still have
  accurate license metadata.
- If a package could be added to upstream GNU Guix with a reasonable amount of
  effort, then it probably doesn't belong in Nonguix.  This isn't a dumping
  ground for subpar packages, but sometimes we may accept free software
  packages which are currently too cumbersome to properly build from source.
- If your package is not a game, you should submit it to the
  [[https://gitlab.com/nonguix/nonguix][Nonguix channel]] instead.
- Game packages are named using their official name, as per [[https://www.wikidata.org/wiki/Wikidata:Main_Page][Wikidata]].  Should
  multiple games share the same name, the first one to be released uses the
  official name and the others have the release date appended to it
  (e.g. "tomb-raider" for the 1996 game and "tomb-raider-2013" for the 2013
  game).
- While many games ship pre-compiled libraries, we try as much as possible to
  replace them with their corresponding Guix inputs.
- All game packages are added to the [[https://gitlab.com/guix-gaming-channels/games][Games channel]] and may include "patches" and
  "fixes."  Games that have a mod community may have extra "patches" that go
  beyond mere fixing: those will go to their dedicated channel (see [[https://gitlab.com/guix-gaming-channels][the Guix
  Gaming Channels group]]).

If you have a history of making quality contributions to GNU Guix or Nonguix
and would like commit access, just ask!  Nontrivial changes should still go
through a simple Merge Request and code review process, but Nonguix needs more
people involved to succeed as a community project.

* Hacking

Games can sometimes be hard to package.  See [[./hacking.org]] for a few tips that
may help you out.  Don't hesitate to contribute more tips to this file!

* Roadmap

This project is still in development and a lot of features are still planned:

- Support for game data sources:
  - Steam
- New build systems to simplify the packaging process:
  - DOSbox
  - Console emulators...
- Importers to automate the packaging process:
  - Steam
  - Humble Bundle
  - Moddb
  - Indiedb
  - Nexusmods

* References

- https://github.com/openlab-aux/vuizvui: Nix recipes for many GOG.com and
  Humble Bundle games.
- https://lutris.net/: Has installation scripts and launchers.
- https://www.protondb.com/: Game support database (for Proton but usually also
  relevant for Wine / DXVK).
- https://www.playonlinux.com/en/supported_apps-1-0.html: Game support database,
  installation scripts, patch database.
- https://appdb.winehq.org/: The Wine Application Database (has useful tips also
  valid for native games).
- https://www.codeweavers.com/compatibility/: The CrossOver database, with
  some tips to fix games.
- https://lgdb.org/: Linux Game Database (down?).
- https://wiki.archlinux.org/index.php/List_of_games: Arch Linux' list of
  (mostly native) games.
- https://liflg.org/: A couple of installers for native games.
- https://icculus.org/
  - https://icculus.org/lgfaq/: Linux Gamers' FAQ.
  - https://icculus.org/lgfaq/gamelist.php: The Linux Gamers' Game List.

DirectX libraries:

- [[https://github.com/iXit/wine-nine-standalone][Gallium Nine]]
- [[https://github.com/disks86/VK9][VK9]] (Inactive)
- [[https://github.com/Joshua-Ashton/d9vk][D9VK]] (now part of DXVK)
- [[https://github.com/doitsujin/dxvk][DXVK]]
